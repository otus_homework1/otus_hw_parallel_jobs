﻿using System.Collections.Concurrent;

namespace OTUS_Parallel_HW
{
    internal class DataPrepare
    {
        private int[] _collection;

        public int[] Collection { get { return _collection; } }
        public DataPrepare(int elementCount)
        {
            _collection = new int[elementCount];

            for (int i = 0; i < elementCount; i++)
            {
                _collection[i] = 1;
            }
        }
    }
}
