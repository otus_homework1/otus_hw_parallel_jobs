﻿using System.Collections.Concurrent;

namespace OTUS_Parallel_HW
{
    internal class MultithreadRunner
    {
        private Barrier startBarrier;
        private Barrier stopBarrier;

        private int result = 0;

        Thread[] threads;
        int[] _array;
        int elPerThread;

        public MultithreadRunner(int[] array, int threadCount)
        {
            _array = array;
            threads = new Thread[threadCount];
            elPerThread = array.Length/threadCount;

            startBarrier = new Barrier(threadCount + 1);
            stopBarrier = new Barrier(threadCount + 1);

            for (int i = 0; i < threadCount; i++)
            {
                var thread = new Thread(SumArray);
                thread.Name = i.ToString();
                thread.Start();
                threads[i] = thread;
            }

        }
        private void SumArray()
        {
            startBarrier.SignalAndWait();

            int res = 0;
            int threadNum = int.Parse(Thread.CurrentThread.Name);

            for(int i = threadNum * elPerThread; i < (threadNum + 1) * elPerThread; i++)
            {
                res += _array[i];
            }

            Interlocked.Add(ref result, res);

            stopBarrier.SignalAndWait();

        }

        public int GetArraySum()
        {
            startBarrier.SignalAndWait();

            stopBarrier.SignalAndWait();

            return result;
        }

    }
}
