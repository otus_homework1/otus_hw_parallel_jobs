﻿
namespace OTUS_Parallel_HW
{
    internal class PLINQ_Runner
    {
        private int[] _array;
        private int result;
        public PLINQ_Runner(int[] array)
        {
            _array = array;
        }

        public int GetArraySum()
        {
            _array
                .AsParallel()
                .ForAll(n => Sum(n));

            return result;
        }

        private void Sum(int i)
        {
            Interlocked.Add(ref result , i);
        }
    }
}
