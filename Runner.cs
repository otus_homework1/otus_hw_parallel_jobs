﻿using System.Collections.Concurrent;

namespace OTUS_Parallel_HW
{
    internal class Runner
    {
        int _multithreadCount;
        int _collectionSize;
        public Runner(int collectionSize, int multithreadCount)
        {
            _collectionSize = collectionSize;
            _multithreadCount = multithreadCount;
        }

        public void Run()
        {

            int[] intCollection = new DataPrepare(_collectionSize).Collection;
            Console.WriteLine($"Коллекция из {_collectionSize} элементов");
            Console.WriteLine();

            var start = DateTime.Now;
            var sum = SingleThreadRunner.GetArraySum(intCollection);
            var end = DateTime.Now;

            Console.WriteLine($"Выполение в одном потоке. Результат: {sum} Время: {(end - start).TotalSeconds} секунд");
            Console.WriteLine();

            var mtr = new MultithreadRunner(intCollection, _multithreadCount);

            start = DateTime.Now;
            sum = mtr.GetArraySum();
            end = DateTime.Now;

            Console.WriteLine($"Выполение в {_multithreadCount} потоках. Результат: {sum} Время: {(end - start).TotalSeconds} секунд");
            Console.WriteLine();

            var plinq = new PLINQ_Runner(intCollection);

            start = DateTime.Now;
            sum = plinq.GetArraySum();
            end = DateTime.Now;

            Console.WriteLine($"Выполение PLINQ. Результат: {sum} Время: {(end - start).TotalSeconds} секунд");
            Console.WriteLine("________________________________________________________________________");
            Console.WriteLine();
        }
    }
}
