﻿using System.Collections.Concurrent;

namespace OTUS_Parallel_HW
{
    internal class SingleThreadRunner
    {
        public static int GetArraySum(int[] array)
        {
            int res = 0;
            for (int i = 0; i < array.Length; i++)
            {
                var take = array[i];
                res += take;
            }

            return res;
        }
    }
}
